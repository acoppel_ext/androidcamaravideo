package com.android.video;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class Componentes {

    /*
        Componente línea
     */
    public static LinearLayout getLinea(Context context)
    {
        //Creando linea
        LinearLayout line = new LinearLayout(context);
        line.setOrientation(LinearLayout.HORIZONTAL);
        line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,1));
        line.setBackgroundColor(Color.rgb(00,42,88));
        line.setPadding(0,5,0,10); //left, top, right, bottom

        return line;
    }

    public static Button getBoton(Context context,String s,int orientacion,int color,boolean negrita,float f)
    {
        //Declaraciones
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(5,10,5,10); // left, top, right, bottom

        Button b = new Button(context);
        b.setText(s);
        b.setBackgroundResource(R.color.color_boton);
        b.setGravity(orientacion);
        b.setLayoutParams(lp);
        b.setTextColor(color);

        if (negrita){
            b.setTypeface(Typeface.DEFAULT_BOLD, Typeface.NORMAL);  //En negrita
        }

        b.setTextSize(f);
        //b.setPadding(Utils.getPaddingBoton(context) * 2,Utils.getPaddingBoton(context),Utils.getPaddingBoton(context) * 2,Utils.getPaddingBoton(context));  //left,top,right,bottom

        return b;
    }
}
