-- TABLE: catrespuestaine
-- DROP TABLE catrespuestaine;
CREATE TABLE catrespuestaine
(
	keyx 		SERIAL NOT NULL,
	fechaalta 	TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
	codigo 		INTEGER NOT NULL DEFAULT -1,
	descripcion 	CHARACTER(150) DEFAULT ''::BPCHAR,
	servicio	CHARACTER(20) DEFAULT ''::BPCHAR,
	accion		CHARACTER(30) DEFAULT ''::BPCHAR,
	mensaje		CHARACTER(300) DEFAULT ''::BPCHAR,	
	CONSTRAINT catrespuestaine_pkey PRIMARY KEY (codigo)
)
WITH (OIDS=FALSE);
ALTER TABLE catrespuestaine OWNER TO sysaforeglobal;
