-- Function: fnrevisarautenticacionineservicios(character)

-- DROP FUNCTION fnrevisarautenticacionineservicios(character);

CREATE OR REPLACE FUNCTION fnrevisarautenticacionineservicios(character)
  RETURNS text AS
$BODY$


DECLARE 
	cCurp 			ALIAS FOR $1;

	cCodigoautenticacion TEXT;
	
BEGIN
	cCodigoautenticacion = '0';
	
	--Obtener el folioconfirma
	SELECT TRIM(folioconfirma) INTO cCodigoautenticacion FROM controlfolioautenticacionineservicios WHERE curp = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1; 
	
	IF cCodigoautenticacion != '' OR cCodigoautenticacion != NULL THEN
		cCodigoautenticacion = '00000000000000';
	ELSE
		SELECT folioautenticacion INTO cCodigoautenticacion FROM controlfolioautenticacionineservicios WHERE curp = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1; 
	END IF;

	-- Devolver el folio de autenticación generado
	RETURN cCodigoautenticacion;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnrevisarautenticacionineservicios(character)
  OWNER TO sysaforeglobal;
