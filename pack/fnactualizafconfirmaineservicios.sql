-- Function: fnactualizafconfirmaineservicios(character, character)

-- DROP FUNCTION fnactualizafconfirmaineservicios(character, character);

CREATE OR REPLACE FUNCTION fnactualizafconfirmaineservicios(
    character,
    character)
  RETURNS integer AS
$BODY$


DECLARE 
	cCurp 					ALIAS FOR $1;
	cFolioautenticacion 	ALIAS FOR $2;

	respuesta integer;
	
BEGIN
	respuesta = 0;
 
	 UPDATE controlfolioautenticacionineservicios SET folioconfirma = cFolioautenticacion WHERE keyx = (SELECT MAX(keyx) FROM controlfolioautenticacionineservicios WHERE curp = cCurp);
	 IF FOUND THEN 
		respuesta = 1;
	 ELSE 
		respuesta = 0;
	 END IF;
	 RETURN respuesta;
		
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnactualizafconfirmaineservicios(character, character)
  OWNER TO sysaforeglobal;
