package com.android.video;

public interface AsyncTaskCompleteListener<T> {

    void onTaskComplete(T result, int id);

}
