package com.android.video;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

public class FTPArchivo extends AsyncTask<String, Void, Void>
{
        private ProgressDialog progressDialog;
        public StringBuilder sb = new StringBuilder();

        public boolean subirArchivo(String ipServer)//String ipServer
        {
            FTPClient ftpClient = new FTPClient();
            BufferedInputStream bufferedInputStream = null;
            String directorio = "/sysx/progs/web/entrada/video";
            ipServer = CamaraActivity.ipAddress.replace("http://", "");

            boolean resultado_store_file = false;

            //dialogo = new Dialogo(CamaraActivity.context, getActivity(), "Por favor, espere mientras se envía el video al servidor",Constantes.DIALOGO_ALERTA,"",4);
            //dialogo.setCancelable(false);
            //dialogo.show();

            // Toast.makeText(CamaraActivity.context,"Por favor, espere mientras se envía el video al servidor", Toast.LENGTH_LONG).show();

            try {
                //ESCRIBIENDO EN EL LOG
                sb.append("CONECTANDO AL SERVIDOR: " + ipServer);

                ftpClient.connect(ipServer, 21);

                boolean login = ftpClient.login("sysafore", "68e071cf9d0cdce309e87477a62206d1");

                while (login == false){
                    login = ftpClient.login("sysafore", "68e071cf9d0cdce309e87477a62206d1");
                }
            /*
            Dialogo dialogo2 = new Dialogo(CamaraActivity.context, getActivity(), "El servidor no responde. Intente de nuevo.",Constantes.DIALOGO_ALERTA,"",4);
            dialogo2.setCancelable(false);
            dialogo2.show();
            */

                //ESCRIBIENDO EN EL LOG
                sb.append("LOGIN AL SERVIDOR: " + login);

                ftpClient.changeWorkingDirectory(directorio);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

                sb.append("CamaraVideoFragment.mNextVideoAbsolutePath: " + CamaraVideoFragment.mNextVideoAbsolutePath);

                if (CamaraVideoFragment.mNextVideoAbsolutePath.isEmpty())
                {
                    sb.append("*************** Esta vacio ************************");
                }else{
                    sb.append("*************** No esta vacio ************************");
                }

                sb.append("CamaraVideoFragment.mNextVideoAbsolutePath: " + CamaraVideoFragment.mNextVideoAbsolutePath);


                File file = new File(CamaraVideoFragment.mNextVideoAbsolutePath);

                if (file.exists()){
                    sb.append("existe");
                }else{
                    sb.append("no existe");
                }



                bufferedInputStream = new BufferedInputStream(new FileInputStream(CamaraVideoFragment.mNextVideoAbsolutePath));
                ftpClient.enterLocalPassiveMode();

                //Contador de intentos al servidor
                int i = 1;

                while (i <= 3)
                {
                    resultado_store_file = ftpClient.storeFile(CamaraVideoFragment.nombreArchivo, bufferedInputStream);

                    //ESCRIBIENDO EN EL LOG
                    sb.append("RESULTADO DEL ENVIO ftpClient.storeFile: " + resultado_store_file);

                    if (resultado_store_file == false) {
                        //ESCRIBIENDO EN EL LOG
                        sb.append("REINTENTO NUMERO " + i + ": " + resultado_store_file);
                        i++;
                    } else {
                        break;
                    }
                }

                if (resultado_store_file) {
                    bufferedInputStream.close();
                    ftpClient.logout();
                    ftpClient.disconnect();
                    CamaraActivity.activity.finishAndRemoveTask();
                    System.exit(0);

                }
                //Se envía el archivo al servidor.
            /*if (resultado_store_file) {
                //Toast.makeText(CamaraActivity.context, "Video enviado al servidor", Toast.LENGTH_LONG).show();

            }else{
                dialogo = new Dialogo(CamaraActivity.context, getActivity(), "El servidor no responde. Intente de nuevo.",Constantes.DIALOGO_ALERTA,"",4);
                dialogo.setCancelable(false);
                dialogo.show();
            }*/

            sb.append("VIDEO ENVIADO AL SERVIDOR DE FORMA EXITOSA. VALOR DE resultado_store_file: " + resultado_store_file);


            } catch (Exception e) {
                Log.i("SERVIDOR sb:  ", sb.toString());
                Log.e("SERVIDOR sb:  ", sb.toString());
            }

            Log.e("SERVIDOR sb:  ", sb.toString());

            return resultado_store_file;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(CamaraActivity.context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            subirArchivo(CamaraActivity.ipAddress); //"http://10.44.172.234"
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            this.progressDialog.dismiss();
            CamaraActivity.activity.setResult(Activity.RESULT_OK);
            CamaraActivity.activity.finish();
            System.exit(0);
        }

}

