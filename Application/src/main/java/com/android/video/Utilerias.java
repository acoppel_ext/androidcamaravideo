package com.android.video;

import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.File;
import java.io.FileOutputStream;


public class Utilerias {
    private static float add = 8;

    public static int getDeviceWidth(Context context)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);

        return metrics.widthPixels;
    }

    public static int getDeviceHeight(Context context)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);

        return metrics.heightPixels;
    }

    static boolean writeFile(File file, byte[] content, boolean append)
    {
        try {
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(content);
            fos.flush();
            fos.close();

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public static boolean Conexion_Internet(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activar_red = cm.getActiveNetworkInfo();
        boolean isConnected = activar_red != null && activar_red.isConnectedOrConnecting();

        return isConnected;
    }

    public static int getMarginAnchoDialogo(Context context){
        int size = 0;
        size = getDeviceWidth(CamaraActivity.context) > 400 ? 400 : 230;

        if (getDeviceWidth(context)> 1000)
        {
            size = 600;
        }

        return size;
    }

    public static float getScaledDensity(Context context)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);

        return metrics.scaledDensity;
    }

    //Texto del menu de opciones
    public static float getSizeFontMenu(Context context)
    {
        float tamano = context.getResources().getDimension(R.dimen.font_size_menu);

        if (getScaledDensity(context) <= 1f)
        {
            tamano = context.getResources().getDimension(R.dimen.font_size_menu) + add;
        }else{
            if(getScaledDensity(context) > 2f)
            {
                tamano = context.getResources().getDimension(R.dimen.font_size_menuBig);
            }
        }

        return tamano;
    }

    public static float getSizeFontFooter(Context context)
    {
        float tamano = context.getResources().getDimension(R.dimen.font_size_footer);

        if (getScaledDensity(context) <= 1f)
        {
            //Resolución menor a 400
            tamano = context.getResources().getDimension(R.dimen.font_size_footer) + add;
        }else{
            if(getScaledDensity(context) > 2f)
            {
                tamano = context.getResources().getDimension(R.dimen.font_size_textBig);
            }
        }

        return tamano;
    }

    //Texto en general
    public static float getSizeFontTextoGeneral(Context context)
    {
        float tamano = context.getResources().getDimension(R.dimen.font_size_text);

        if (getScaledDensity(context) <= 1f)
        {
            //Resolución menor a 400
            tamano = context.getResources().getDimension(R.dimen.font_size_text) + add;
        }else{
            if(getScaledDensity(context) > 2f)
            {
                tamano = context.getResources().getDimension(R.dimen.font_size_textBig);
            }
        }

        return tamano;
    }

    public static void AjustarVolumenMicrofono(Context context){
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        am.setStreamVolume(AudioManager.STREAM_MUSIC,am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),0);
    }
}
