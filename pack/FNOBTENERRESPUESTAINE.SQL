-- FUNCTION: fnobtenerrespuestaine(bigint)
-- DROP FUNCTION fnobtenerrespuestaine(bigint);

CREATE OR REPLACE FUNCTION fnobtenerrespuestaine(bigint)
  RETURNS SETOF tpobtenerrespuestaine AS
$BODY$

DECLARE
	iFolioIne 	ALIAS FOR $1;
	ikeyx		INTEGER;
	dtResp 		tpobtenerrespuestaine;

BEGIN
	dtResp.respuesta 	= 0;
	dtResp.codigo 		= 0;
	dtResp.json 		= '';
	ikeyx			= 0;
	--Obtener el keyx en base al folio solicitud de la api
	SELECT BX.keyx INTO ikeyx
	FROM tbverificadatosenviarine AS AX
	INNER JOIN ineautenticacionhuellas AS BX ON (AX.curp = BX.curp)
	WHERE foliosolicitud = iFolioIne
	ORDER BY BX.fechaalta DESC LIMIT 1;
	--Actualizar en la tabla bitacora el folio de la solicitud de la ine
	UPDATE ineautenticacionhuellas SET foliosoline = iFolioIne WHERE keyx = ikeyx;

	IF EXISTS (SELECT 'X' FROM tbverificaciondatosrespuestaine WHERE foliosolicitud = iFolioIne) THEN
		SELECT 1,codigorespuesta,datosrespuesta INTO dtResp FROM tbverificaciondatosrespuestaine WHERE foliosolicitud = iFolioIne;
	END IF;

	RETURN NEXT dtResp;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fnobtenerrespuestaine(bigint) OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnobtenerrespuestaine(bigint) TO sysaforeglobal;