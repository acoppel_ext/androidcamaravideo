package com.android.video;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;


public class Eventos {
    /*
     * Este método se utiliza cuando el usuario presiona el botón Ok del cuadro de dialogo de CONFIRMACION
     */
    public void EventoFinalizar(final Context context, final Activity a, final Dialog dialog, final Button boton, final String opcion)
    {
        boton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                dialog.cancel();
                dialog.dismiss();


                File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + CamaraVideoFragment.nombreArchivo);

                if (file.exists()){
                    file.delete();
                }

                //Se envía un RESULT_CANCELED para indicar que se sale de la app.
                a.setResult(Activity.RESULT_CANCELED);
                a.finish();
                System.exit(0);
            }
        });
    }

    /*
     * Este método se utiliza cuando el usuario presiona el botón Cancelar del cuadro de dialogo de CONFIRMACION
     */
    public void EventoCancelarDialogo(final Context context,final Dialog dialog, final Button boton)
    {
        boton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                //Cierro la ventana del Dialogo
                dialog.cancel();
                dialog.dismiss();

                //Coloco visible el botón Iniciar Grabación
                CamaraVideoFragment.mButtonVideo.setVisibility(View.VISIBLE);
                CamaraVideoFragment.tv_iniciar.setVisibility(View.VISIBLE);

                //Coloco visible el botón Salir
                CamaraActivity.mSalir.setVisibility(View.VISIBLE);
                CamaraActivity.tv_salir.setVisibility(View.VISIBLE);

                //Verifico cuál es la opción activa para regresar el estado anterior.

                switch(CamaraActivity.opcion_activa_app){
                    //Opción activa Grabar
                    case 1:  //Toast.makeText(CamaraActivity.context, "duracion: " + CamaraActivity.contador, Toast.LENGTH_SHORT).show();
                        //Colocar visible el botón Reproducir si el archivo es valido mayor de 10 segundos

                        if (CamaraActivity.video_valido_min_time)
                        {
                            CamaraVideoFragment.mReproducirVideo.setVisibility(View.VISIBLE);
                            CamaraVideoFragment.tv_reproducir.setVisibility(View.VISIBLE);
                        }
                        break;

                    //Opción activa Detener
                    case 2:     if (CamaraActivity.video_valido_min_time)
                    {
                        //Oculto el botón Reproducir por que no es un video válido.
                        //El video es menor a 10 segundos.
                        CamaraVideoFragment.mReproducirVideo.setVisibility(View.VISIBLE);
                        CamaraVideoFragment.tv_reproducir.setVisibility(View.VISIBLE);
                    }else{
                        CamaraVideoFragment.mReproducirVideo.setVisibility(View.INVISIBLE);
                        CamaraVideoFragment.tv_reproducir.setVisibility(View.INVISIBLE);
                    }
                        break;

                    //Opción activa Reproducir
                    case 3:    // Toast.makeText(CamaraActivity.context, "Opción Reproducir: " + CamaraActivity.opcion_activa_app, Toast.LENGTH_SHORT).show();

                        //Colocar visible el botón Reproducir si el archivo es valido mayor de 10 segundos
                        if (CamaraActivity.video_valido_min_time)
                        {
                            CamaraVideoFragment.mReproducirVideo.setVisibility(View.VISIBLE);
                            CamaraVideoFragment.tv_reproducir.setVisibility(View.VISIBLE);
                        }

                        break;

                    //Opción activa Guardar
                    case 4: break;

                    //Opción activa Salir
                    case 5: // Toast.makeText(CamaraActivity.context,CamaraActivity.opcion_activa_app, Toast.LENGTH_SHORT).show();

                        if ((CamaraActivity.opcion_activa_app == Constantes.activo_boton_grabar)||
                                (CamaraActivity.opcion_activa_app == Constantes.activo_boton_regrabar)) {

                        }
                        break;

                    //Opción activa Regrabar
                    case 6:
                        if (CamaraActivity.video_valido_min_time != false){
                            //Colocar visible el botón Reproducir
                            CamaraVideoFragment.mReproducirVideo.setVisibility(View.VISIBLE);
                            CamaraVideoFragment.tv_reproducir.setVisibility(View.VISIBLE);
                        }else{
                            //Colocar invisible el botón Reproducir
                            CamaraVideoFragment.mReproducirVideo.setVisibility(View.INVISIBLE);
                            CamaraVideoFragment.tv_reproducir.setVisibility(View.INVISIBLE);
                        }

                        break;

                    default: break;
                }
            }
        });
    }

    /*
     * Este método se utiliza cuando el usuario presiona el botón Aceptar del cuadro de dialogo de ALERTA
     */
    public void EventoOkAcepto(final Context context, final Activity a, final Dialog dialog, final Button boton, final String opcion){

        boton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                //Toast.makeText(CamaraActivity.context, "dialogo alerta: " + CamaraActivity.opcion_activa_app, Toast.LENGTH_SHORT).show();

                switch(CamaraActivity.opcion_activa_app){
                    //Opción activa Grabar
                    case 1:
                        //Toast.makeText(CamaraActivity.context, "CamaraActivity.video_valido_min_time: " + CamaraActivity.video_valido_min_time, Toast.LENGTH_SHORT).show();
                        break;

                    //Opción activa Detener
                    case 2: break;

                    //Opción activa Reproducir
                    case 3: //Colocar visible el Botón Grabar
                        CamaraVideoFragment.mButtonVideo.setVisibility(View.VISIBLE);
                        CamaraVideoFragment.tv_iniciar.setVisibility(View.VISIBLE);

                        //Colocar visible el botón Reproducir si el archivo es valido mayor de 10 segundos
                        if (CamaraActivity.video_valido_min_time)
                        {
                            CamaraVideoFragment.mReproducirVideo.setVisibility(View.VISIBLE);
                            CamaraVideoFragment.tv_reproducir.setVisibility(View.VISIBLE);
                        }

                        dialog.cancel();
                        dialog.dismiss();
                        break;

                    //Opción activa Guardar
                    case 4: dialog.cancel();
                        dialog.dismiss();

                        File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + CamaraVideoFragment.nombreArchivo);

                        if (file.exists()){
                            file.delete();
                        }
                        //Se envía un OK para indicar que se sale de la app.
                        a.setResult(Activity.RESULT_OK);
                        a.finish();
                        System.exit(0);
                        break;

                    //Opción activa Salir
                    case 5: dialog.cancel();
                        dialog.dismiss();

                        File file2 = new File(Environment.getExternalStorageDirectory().toString() + "/" + CamaraVideoFragment.nombreArchivo);

                        if (file2.exists()){
                            file2.delete();
                        }

                        CamaraActivity.mSalir.setVisibility(View.INVISIBLE);
                        CamaraActivity.tv_salir.setVisibility(View.INVISIBLE);

                        //Se envía un RESULT_CANCELED para indicar que se sale de la app.
                        a.setResult(Activity.RESULT_CANCELED);
                        a.finish();
                        System.exit(0);
                        break;

                    //Opción activa Regrabar
                    case 6: break;

                    default: dialog.cancel();
                        dialog.dismiss();
                        break;
                }
            }

        });
    }
}
