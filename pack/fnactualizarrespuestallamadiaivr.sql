-- Function: fnactualizarrespuestallamadiaivr(integer)

-- DROP FUNCTION fnactualizarrespuestallamadiaivr(integer)

CREATE OR REPLACE FUNCTION fnactualizarrespuestallamadiaivr(integer)
  RETURNS integer AS
$BODY$
DECLARE 
	iFolioIne ALIAS FOR $1;
	iActualizo INTEGER;
BEGIN
	iActualizo = 0;
	
	UPDATE tbenviosmsvalidacionine SET respuesta = 1, intentos = (intentos + 1) where foliosolicitudine = iFolioIne;
	IF FOUND THEN
		iActualizo = 1;
	END IF;

	RETURN iActualizo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION fnactualizarrespuestallamadiaivr(integer)
  OWNER TO sysaforeglobal;