-- TABLE: catrespuestaineservicios
-- DROP TABLE catrespuestaineservicios;
CREATE TABLE catrespuestaineservicios
(
	keyx 		SERIAL NOT NULL,
	fechaalta 	TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
	codigo 		INTEGER NOT NULL DEFAULT -1,
	descripcion 	CHARACTER(150) DEFAULT ''::BPCHAR,
	servicio	CHARACTER(20) DEFAULT ''::BPCHAR,
	accion		CHARACTER(30) DEFAULT ''::BPCHAR,
	mensaje		CHARACTER(300) DEFAULT ''::BPCHAR,	
	CONSTRAINT catrespuestaineservicios_pkey PRIMARY KEY (codigo)
)
WITH (OIDS=FALSE);
ALTER TABLE catrespuestaineservicios OWNER TO sysaforeglobal;