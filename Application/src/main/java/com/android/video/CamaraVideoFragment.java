package com.android.video;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import com.android.video.pojo.rotarVideo;
import com.android.video.pojo.infoVideo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CamaraVideoFragment extends Fragment
        implements View.OnClickListener, FragmentCompat.OnRequestPermissionsResultCallback {

    //private TextView tv_iniciar, tv_detener, tv_reproducir, tv_guardar;
    private FTPUpload oEnvioFTP;
    APIInterface apiInterface;
    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    private static final String TAG = "CamaraVideoFragment";
    private static final int REQUEST_VIDEO_PERMISSIONS = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    public static ImageButton mButtonVideo, mDetenerGrabacion, mReproducirVideo, mVideoTerminado;
    public static TextView tv_iniciar, tv_detener, tv_reproducir, tv_guardar, tv_salir; //textView;

    private int progressStatus = 0;
    private TextView textView;
    private Handler handler = new Handler();

    private Integer mSensorOrientation;
    public static String mNextVideoAbsolutePath;
    public static String nombreArchivo, ipAddress, texto, iTiempoMinimo, iTiempoMaximo;
    private CaptureRequest.Builder mPreviewBuilder;
    private TextView sTextoCliente;
    public StringBuilder sb = new StringBuilder();


    private static final String[] VIDEO_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
    };

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    public static Dialogo dialogo;

    /**
     * Un {@link AutoFitTextureView} para el preview de la camara.
     */
    // private AutoFitTextureView mTextureView;
    private TextureView mTextureView;

    /**
     * Objeto donde se visualizaran los videos antes de subirlos a nuestros servidores
     */
    private VideoView mVideoPrevisualizacion;

    /**
     * Botón para grabar el video.
     */
    //private ImageButton mButtonVideo;

    /**
     * Boton para detener la grabacion del video
     * */
    //private ImageButton mDetenerGrabacion;

    /**
     * Boton para reproducir el video
     */
    //private ImageButton mReproducirVideo;

    /**
     * Boton para enviar el video al servidor FTP
     */
    //private ImageButton mVideoTerminado;

    /**
     * Texto que leera el trabajador para ceder su afore.
     */
    private TextView mTextoLecturaTrabajador;

    /**
     * Una referencia a opened {@link android.hardware.camera2.CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * Una referencia a current {@link android.hardware.camera2.CameraCaptureSession} para preview.
     */
    private CameraCaptureSession mPreviewSession;

    //Cronómetro
    public static Chronometer cronometro;

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */

    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture,
                                              int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture,
                                                int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

    };

    /**
     * El {@link android.util.Size} preview de la camara.
     */
    private Size mPreviewSize;

    /**
     * El {@link android.util.Size} del video grabado.
     */
    private Size mVideoSize;

    /**
     * MediaRecorder
     */
    public static MediaRecorder mMediaRecorder;

    /**
     * Cuando la app está grabando video ahora.
     */
    public static boolean mIsRecordingVideo;

    /**
     * Un thread adicional para correr tareas que no deben bloquear la UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * Un {@link Handler} para correr las tareas en el background.
     */
    private Handler mBackgroundHandler;

    /**
     * Un {@link Semaphore} para prevenir la app existente antes cerrar la cámara.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * {@link CameraDevice.StateCallback} es llamado cuando {@link CameraDevice} cambian el estatus.
     */
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback()
    {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            startPreview();
            mCameraOpenCloseLock.release();
            if (null != mTextureView) {
                configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                activity.setResult(Activity.RESULT_CANCELED);
                activity.finish();
                //System.exit(0);
            }
        }
    };

    public static CamaraVideoFragment newInstance() {
        return new CamaraVideoFragment();
    }

    /**
     *
     * @param choices The list of available sizes
     * @return The video size
     */
    private static Size chooseVideoSize(Size[] choices) {
        for (Size size : choices) {
            if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
                return size;
            }
        }
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle parametros = getArguments();
        nombreArchivo = CamaraActivity.nombre_archivo;//parametros.getString("nombre_archivo");
        nombreArchivo = nombreArchivo.replace(".webm" , ".mp4");
        texto = CamaraActivity.texto;//parametros.getString("texto");
        iTiempoMinimo = CamaraActivity.iTiempoMinimo;
        iTiempoMaximo = CamaraActivity.iTiempoMaximo;
        ipAddress = CamaraActivity.ipAddress; //parametros.getString("ipAddress");

        return inflater.inflate(com.android.video.R.layout.fragment_camera2_video, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        mTextureView = (TextureView) view.findViewById(R.id.texture);
        mVideoPrevisualizacion = (VideoView) view.findViewById(R.id.videoPrevisualizacion);
        sTextoCliente = view.findViewById(R.id.instruccion);

        cronometro = (Chronometer) view.findViewById(R.id.cronometro);
        mTextoLecturaTrabajador = (TextView) view.findViewById(R.id.texto_lectura_trabajador);

        mButtonVideo = (ImageButton) view.findViewById(R.id.grabarVideo);
        mDetenerGrabacion = (ImageButton) view.findViewById(R.id.detenerGrabacion);
        mReproducirVideo = (ImageButton) view.findViewById(R.id.reproducirVideo);
        mVideoTerminado = (ImageButton) view.findViewById(R.id.videoTerminado);

        //Textos de los botones
        tv_iniciar = (TextView) view.findViewById(R.id.textoGrabarVideo);
        tv_detener = (TextView) view.findViewById(R.id.textoDetenerVideo);
        tv_reproducir = (TextView) view.findViewById(R.id.textoReproducirVideo);
        tv_guardar = (TextView) view.findViewById(R.id.textoGuardarVideo);



        //Evento click
        mButtonVideo.setOnClickListener(this);
        mDetenerGrabacion.setOnClickListener(this);
        mReproducirVideo.setOnClickListener(this);
        mVideoTerminado.setOnClickListener(this);
        CamaraActivity.mSalir.setOnClickListener(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Colocando el texto del cliente
        sTextoCliente.setText(texto);

        if (sTextoCliente.getText().toString().trim().length() == 0)
        {
            sTextoCliente.setText("Texto del Cliente");
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        startBackgroundThread();
        if (mTextureView.isAvailable()) {
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.grabarVideo: {
                try {
                    //Reseteando a false la variable que indica si es válido el video en tiempo
                    CamaraActivity.video_valido_min_time = false;

                    //Se ajusta el volumen del micrófono
                    Utilerias.AjustarVolumenMicrofono(CamaraActivity.context);

                    //Cambiar el nombre del botón a "Volver a Grabar"
                    if(CamaraActivity.volver_grabar == false){
                        //Opción Grabar
                        CamaraActivity.opcion_activa_app = Constantes.activo_boton_grabar;
                        tv_iniciar.setText(R.string.txt_grabar);
                        CamaraActivity.volver_grabar = true;

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(60,0,0,0);
                        tv_iniciar.setLayoutParams(params);


                    }else{
                        //Opción ReGrabar
                        CamaraActivity.opcion_activa_app = Constantes.activo_boton_regrabar;
                        tv_iniciar.setText(R.string.txt_volver_grabar);
                    }

                    ocultarBotonGrabarVideo();
                    startRecordingVideo();
                }catch(Exception e){}
                break;
            }
            case R.id.detenerGrabacion: {
                try {
                    //Opción Detener
                    CamaraActivity.opcion_activa_app = Constantes.activo_boton_detener;
                    ocultarBotonDetenerGrabacion();
                    stopRecordingVideo();
                }catch(Exception e){
                }
                break;
            }
            case R.id.reproducirVideo: {
                try {
                    //Opción Reproducir
                    CamaraActivity.opcion_activa_app = Constantes.activo_boton_reproducir;
                    //Toast.makeText(CamaraActivity.context, "Se inicia la reproducción del video...", Toast.LENGTH_LONG).show();
                    reproducirVideo();

                }catch(Exception e){
                }
                break;
            }
            case R.id.videoTerminado: {
                try {
                    //Opción activa Guardar
                    CamaraActivity.opcion_activa_app = Constantes.activo_boton_guardar;

                    //Botón Iniciar grabación
                    mButtonVideo.setVisibility(View.INVISIBLE);
                    tv_iniciar.setVisibility(View.INVISIBLE);

                    //Botón Detener grabación
                    mDetenerGrabacion.setVisibility(View.INVISIBLE);
                    tv_detener.setVisibility(View.INVISIBLE);

                    //Botón Reproducir
                    mReproducirVideo.setVisibility(View.INVISIBLE);
                    tv_reproducir.setVisibility(View.INVISIBLE);

                    //Botón Guardar video al servidor
                    mVideoTerminado.setVisibility(View.INVISIBLE);
                    tv_guardar.setVisibility(View.INVISIBLE);

                    //Botón Salir de la app
                    CamaraActivity.mSalir.setVisibility(View.INVISIBLE);
                    CamaraActivity.tv_salir.setVisibility(View.INVISIBLE);

                    //Se oculta el cronometro
                    cronometro.setBackgroundColor(Color.TRANSPARENT);
                    cronometro.setVisibility(View.INVISIBLE);

                    //FTP ftp_upload = new FTP();  //"archivo.webm","/sysx/smbx/audios/"
                    //Enviando el audio al servidor con el método subirArchivo.

                    //Subir archivo
                    //boolean videoE<nviado = subirArchivo("http://10.44.172.234");

                    /*
                    //Si el video fue enviado al servidor
                    if (videoEnviado)
                    {
                        //El video fue enviado al servidor y se borra del almacenamiento local.
                        //File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + "audio.webm");
                        //file.delete();
                        //inicializarApp();
                    }
                       */

                    if (Utilerias.Conexion_Internet(CamaraActivity.context) == false)
                    {
                        //No ha conexión a la Red
                        dialogo = new Dialogo(CamaraActivity.context, getActivity(), Constantes.etiqNoConexionInternet,Constantes.DIALOGO_ALERTA,"",-1);
                        dialogo.setCancelable(false);
                        dialogo.show();

                    }else{
                        mDetenerGrabacion.setVisibility(View.INVISIBLE);
                        tv_detener.setVisibility(View.INVISIBLE);

                        FTPUpload tSubirArchivo = new FTPUpload();
                        tSubirArchivo.execute(ipAddress);

                        // FTPArchivo ftp_archivo = new FTPArchivo();
                        // ftp_archivo.subirArchivo("http://10.44.172.234");
                    }

                    // enviarVideoFtp();

                }catch(Exception e){
                    e.printStackTrace();
                }

                break;
            }

            case R.id.salir:{
                try {
                    //Opción activa Guardar
                    CamaraActivity.opcion_activa_app = Constantes.activo_boton_salir;

                    //Toast.makeText(CamaraActivity.context, "Opción activa app en salir: " + CamaraActivity.opcion_activa_app, Toast.LENGTH_SHORT).show();

                    //Se verifica la opción activada por el usuario
                    switch(CamaraActivity.opcion_activa_app){
                        //Opción Salir inicial
                        case 0: dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                            dialogo.setCancelable(false);
                            dialogo.show();
                            break;

                        //Opción Grabar y Regrabar
                        case 1: case 6:     mReproducirVideo.setVisibility(View.INVISIBLE);
                            tv_reproducir.setVisibility(View.INVISIBLE);

                            if ((CamaraActivity.opcion_activa_app == Constantes.activo_boton_grabar)||
                                    (CamaraActivity.opcion_activa_app == Constantes.activo_boton_regrabar))
                            {
                                ocultarBotonDetenerGrabacion();
                                stopRecordingVideo();

                                dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                                dialogo.setCancelable(false);
                                dialogo.show();

                                if (CamaraActivity.video_valido_min_time == false)
                                {
                                    //Oculto el botón Reproducir por que no es un video válido.
                                    //El video es menor a 10 segundos.
                                    mReproducirVideo.setVisibility(View.INVISIBLE);
                                    tv_reproducir.setVisibility(View.INVISIBLE);
                                }

                            }else{
                                mReproducirVideo.setVisibility(View.INVISIBLE);
                                tv_reproducir.setVisibility(View.INVISIBLE);

                                dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                                dialogo.setCancelable(false);
                                dialogo.show();
                            }

                            break;

                        case 2: mReproducirVideo.setVisibility(View.VISIBLE);
                            tv_reproducir.setVisibility(View.VISIBLE);
                            dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                            dialogo.setCancelable(false);
                            dialogo.show();
                            break;

                        //Opción Reproducir
                        case 3: //Se detiene la reproducción del video.
                            mVideoPrevisualizacion.stopPlayback();
                            mReproducirVideo.setVisibility(View.VISIBLE);
                            tv_reproducir.setVisibility(View.VISIBLE);
                            dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                            dialogo.setCancelable(false);
                            dialogo.show();
                            break;

                        //Opción Guardar
                        case 4: break;

                        //Opción Detener y Salir

                        case 5:  //Si se está grabando se detiene el video antes de salir.
                            if (mIsRecordingVideo){
                                ocultarBotonDetenerGrabacion();
                                stopRecordingVideo();
                            }

                            mReproducirVideo.setVisibility(View.INVISIBLE);
                            tv_reproducir.setVisibility(View.INVISIBLE);

                            dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                            dialogo.setCancelable(false);
                            dialogo.show();
                            break;

                        default:
                    }

                    //Opcion activa Salir
                    //CamaraActivity.opcion_activa_app = Constantes.activo_boton_salir;

                    /*dialogo = new Dialogo(CamaraActivity.context,getActivity(),Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
                    dialogo.setCancelable(false);
                    dialogo.show();*/

                }catch(Exception e){
                    //Toast.makeText(CamaraActivity.context, "No es posible enviar el video al servidor.", Toast.LENGTH_LONG).show();
                }

                break;
            }
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtiene si debe mostrar la interfaz de usuario con una justificación para solicitar permisos.
     *
     * @param permissions Los permisos que tu aplicación quiere solicitar.
     * @return Si puede mostrar el permiso de la interfaz de usuario.
     */
    private boolean shouldShowRequestPermissionRationale(String[] permissions) {
        for (String permission : permissions) {
            if (FragmentCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Solicitud de permisos requeridos para grabar el video.
     */
    private void requestVideoPermissions() {
        if (shouldShowRequestPermissionRationale(VIDEO_PERMISSIONS)) {
            new ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(this, VIDEO_PERMISSIONS, REQUEST_VIDEO_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult");
        if (requestCode == REQUEST_VIDEO_PERMISSIONS) {
            if (grantResults.length == VIDEO_PERMISSIONS.length) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        ErrorDialog.newInstance(getString(com.android.video.R.string.permission_request))
                                .show(getChildFragmentManager(), FRAGMENT_DIALOG);
                        break;
                    }
                }
            } else {
                ErrorDialog.newInstance(getString(com.android.video.R.string.permission_request))
                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean hasPermissionsGranted(String[] permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(getActivity(), permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     * Intenta abrir un {@link CameraDevice}. El resultado es escuchado por `mStateCallback`.
     */
    @SuppressWarnings("MissingPermission")
    private void openCamera(int width, int height) {
        if (!hasPermissionsGranted(VIDEO_PERMISSIONS)) {
            requestVideoPermissions();
            return;
        }
        final Activity activity = getActivity();
        if (null == activity || activity.isFinishing()) {
            return;
        }
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {

            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Se agotó el tiempo de espera para el desbloqueo de la cámara.");
            }
            // Para seleccionar la camara trasera es el index "0" y la camara frontal es el "1"
            String cameraId = manager.getCameraIdList()[1];

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            if (map == null) {
                throw new RuntimeException("No está disponible el preview del tamaño del video.");
            }
            mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    width, height, mVideoSize);

            int orientation = getResources().getConfiguration().orientation;
            // modificado
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                // mTextureView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                // mTextureView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }
            configureTransform(width, height);
            mMediaRecorder = new MediaRecorder();
            manager.openCamera(cameraId, mStateCallback, null);
        } catch (CameraAccessException e) {
            //Toast.makeText(activity, "No se puede acceder a la cámara.", Toast.LENGTH_LONG).show();
            activity.setResult(Activity.RESULT_CANCELED);
            activity.finish();
            //System.exit(0);
        } catch (NullPointerException e) {
            ErrorDialog.newInstance(getString(com.android.video.R.string.camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrumpido al intentar bloquear la apertura de la cámara.");
        }
    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            closePreviewSession();
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mMediaRecorder) {
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrumpido al intentar bloquear la cámara de cierre.");
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * @return void
     * Iniciando el preview de la cámara.
     */
    private void startPreview() {
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            Surface previewSurface = new Surface(texture);
            mPreviewBuilder.addTarget(previewSurface);

            mCameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Activity activity = getActivity();
                            if (null != activity) {
                                //Toast.makeText(activity, "Falló", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Actualizar la vista previa de la cámara {@link #startPreview()} necesita ser llamado por adelantado.
     */
    private void updatePreview() {
        if (null == mCameraDevice) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
            HandlerThread thread = new HandlerThread("CameraPreview");
            thread.start();
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
    }

    /**
     * Configura la transformación {@link android.graphics.Matrix} necesaria para `mTextureView`.
     * Este método no debe llamarse hasta que el tamaño de vista previa de la cámara se determine en
     * openCamera, o hasta que el tamaño de `mTextureView` sea fijo.
     *
     * @param viewWidth  el ancho de `mTextureView`
     * @param viewHeight el alto de `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        Activity activity = getActivity();
        if (null == mTextureView || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();

        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }

        mTextureView.setTransform(matrix);
    }

    private void setUpMediaRecorder() throws IOException {
        final Activity activity = getActivity();

        if (null == activity) {
            return;
        }

        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

        if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath.isEmpty())
        {
            mNextVideoAbsolutePath = getVideoArchivoPath(getActivity());
        }

        mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
        mMediaRecorder.setVideoEncodingBitRate(10000000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        //int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(1));

        mMediaRecorder.prepare();
    }

    /*
     * @param N/A
     * @return void
     * Método startRecordingVideo()inicia la grabación del video.
     */
    private void startRecordingVideo()
    {
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }

        int sVisibilidadTexture = mTextureView.getVisibility();
        boolean camaraActiva = sVisibilidadTexture == View.VISIBLE;

        if (!camaraActiva) {
            mTextureView.setVisibility(View.VISIBLE);
            mVideoPrevisualizacion.setVisibility(View.INVISIBLE);
        }

        try {
            closePreviewSession();
            setUpMediaRecorder();
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            List<Surface> surfaces = new ArrayList<>();

            // Configurar superficie para la vista previa de la cámara
            Surface previewSurface = new Surface(texture);
            surfaces.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);

            // Configurar Surface para el MediaRecorder
            Surface recorderSurface = mMediaRecorder.getSurface();
            surfaces.add(recorderSurface);
            mPreviewBuilder.addTarget(recorderSurface);

            cronometro.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer cronometro)
                {
                    long systemCurrTime = SystemClock.elapsedRealtime();
                    long chronometerBaseTime = cronometro.getBase();
                    long deltaTime = systemCurrTime - chronometerBaseTime;

                    //  Toast.makeText(CamaraActivity.context, "****** deltaTime: " + deltaTime/6000, Toast.LENGTH_SHORT).show();

                    try {
                        //El tiempo es menor a 10 segundos.
                        if (deltaTime < Integer.parseInt(iTiempoMinimo)*1000 ) {
                            //Ocultar el botón Detener
                            mDetenerGrabacion.setVisibility(View.INVISIBLE);
                            tv_detener.setVisibility(View.INVISIBLE);

                            //Ocultar el botón Reproducir
                            mReproducirVideo.setVisibility(View.INVISIBLE);
                            tv_reproducir.setVisibility(View.INVISIBLE);

                            //Se indica que el video no es válido por ser menor a 10 segundos.
                            //CamaraActivity.video_valido_min_time = false;

                            //Toast.makeText(CamaraActivity.context, "Video menor a 10 segundos: " + CamaraActivity.video_valido_min_time, Toast.LENGTH_SHORT).show();

                        }else{  //Tiempo superior a 10 segundos

                            //Se indica que el video es válido mayor a 10 segundos
                            // CamaraActivity.video_valido_min_time = true;

                            //Colocar visible el botón Reproducir
                            //mReproducirVideo.setVisibility(View.VISIBLE);
                            //tv_reproducir.setVisibility(View.VISIBLE);

                            //Toast.makeText(CamaraActivity.context, "Video mayor a 10 segundos: " + CamaraActivity.video_valido_min_time, Toast.LENGTH_SHORT).show();

                            if (CamaraActivity.opcion_activa_app != Constantes.activo_boton_reproducir)
                            {
                                CamaraActivity.video_valido_min_time = true;

                                //La opción activa no es reproducción

                                if (CamaraActivity.opcion_activa_app == Constantes.activo_boton_guardar){
                                    mDetenerGrabacion.setVisibility(View.INVISIBLE);
                                    tv_detener.setVisibility(View.INVISIBLE);
                                }else{
                                    mDetenerGrabacion.setVisibility(View.VISIBLE);
                                    tv_detener.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                        //El tiempo es igual 120 segundos.
                        if (deltaTime > Integer.parseInt(iTiempoMaximo)*1000) {

                            CamaraActivity.video_valido_min_time = true;

                            // Toast.makeText(CamaraActivity.context, "Video mayor a 120 segundos: " + CamaraActivity.video_valido_min_time, Toast.LENGTH_SHORT).show();

                            //Reinicia el contador cada 120 segundos
                            ocultarBotonDetenerGrabacion();
                            stopRecordingVideo();

                            //Cambiar el nombre del botón a "Volver a Grabar"
                            if(CamaraActivity.volver_grabar == false){
                                tv_iniciar.setText(R.string.txt_volver_grabar);
                                CamaraActivity.volver_grabar = true;
                            }

                            //Toast.makeText(CamaraActivity.context, "Video guardado. ", Toast.LENGTH_SHORT).show();
                        }

                    }catch(Exception e){
                    }
                }
            });

            // Iniciando una sesión de captura
            // Una vez la sesión inicada, se actualiza la UI y se inicia la grabación.
            mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    mPreviewSession = cameraCaptureSession;
                    updatePreview();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // UI
                            // mButtonVideo.setText(com.example.android.camera2video.R.string.stop);
                            mIsRecordingVideo = true;

                            //Cronometro
                            cronometro.setFormat(" Grabando: %s ");
                            cronometro.setBackgroundColor(Color.TRANSPARENT);
                            cronometro.setTextColor(Color.BLACK);

                            //Para resetear el cronometro al iniciar la grabación
                            cronometro.setBase(SystemClock.elapsedRealtime());

                            //Cronometro visible
                            cronometro.setVisibility(View.VISIBLE);

                            //Iniciando el cronómetro
                            cronometro.start();

                            // Iniciando grabación
                            mMediaRecorder.start();

                            //Oculto los botones reproducir y guardar al servidor
                            mReproducirVideo.setVisibility(View.INVISIBLE);
                            tv_reproducir.setVisibility(View.INVISIBLE);

                            mVideoTerminado.setVisibility(View.INVISIBLE);
                            tv_guardar.setVisibility(View.INVISIBLE);
                        }
                    });
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Activity activity = getActivity();
                    if (null != activity) {
                        //Toast.makeText(activity, "Falló", Toast.LENGTH_LONG).show();
                    }
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    private void closePreviewSession() {
        if (mPreviewSession != null) {
            mPreviewSession.close();
            mPreviewSession = null;
        }
    }

    void stopRecordingVideo()
    {
        // UI
        mIsRecordingVideo = false;

        //Stop cronometro
        cronometro.stop();

        // Stop recording
        mMediaRecorder.stop();
        mMediaRecorder.reset();

        tv_iniciar.setText(R.string.txt_volver_grabar);
        CamaraActivity.volver_grabar = true;

        Activity activity = getActivity();

        // mNextVideoAbsolutePath = null;

        startPreview();
    }

    /**
     *
     * Proceso que inicializa los valores de todos los elementos en la grabación. del video
     */
    public void inicializarApp()
    {
        nombreArchivo = null;
        mNextVideoAbsolutePath = null;
        mTextureView.setVisibility(View.VISIBLE);
        mVideoPrevisualizacion.setVisibility(View.INVISIBLE);

        //Botón Iniciar Grabación de video
        mButtonVideo.setVisibility(View.VISIBLE);
        tv_iniciar.setVisibility(View.VISIBLE);

        //Botón Detener grabación de video
        mDetenerGrabacion.setVisibility(View.INVISIBLE);
        tv_detener.setVisibility(View.INVISIBLE);

        mReproducirVideo.setVisibility(View.INVISIBLE);
        tv_reproducir.setVisibility(View.INVISIBLE);

        mVideoTerminado.setVisibility(View.INVISIBLE);
        tv_guardar.setVisibility(View.INVISIBLE);
    }

    /**
     * Oculta el boton que inicializa la grabacion de audio y muestra el de detener grabacion
     *
     * */
    public void ocultarBotonGrabarVideo()
    {
        mButtonVideo.setVisibility(View.INVISIBLE);
        tv_iniciar.setVisibility(View.INVISIBLE);
    }

    /**
     * Oculta el boton detener grabacion y muestra el boton reproducir video y el boton que
     * acepta el video y lo sube al servidor
     * */
    public void ocultarBotonDetenerGrabacion()
    {
        //Botón Detener Grabación de Video
        mDetenerGrabacion.setVisibility(View.INVISIBLE);
        tv_detener.setVisibility(View.INVISIBLE);

        //Botón Grabar video
        mButtonVideo.setVisibility(View.VISIBLE);
        tv_iniciar.setVisibility(View.VISIBLE);

        //Botón Reproducir Video
        mReproducirVideo.setVisibility(View.VISIBLE);
        tv_reproducir.setVisibility(View.VISIBLE);

        mVideoTerminado.setVisibility(View.INVISIBLE);
        tv_guardar.setVisibility(View.INVISIBLE);
    }

    /**
     *
     * Reproduce el video que previamente se grabó donde el cliente expresa
     * que es su voluntad ingresar al afore coppel
     */
    /*
     * @param N/A
     * @return void
     * Método reproducirVideo() inicia la reproducción del video.
     */
    public void reproducirVideo()
    {
        int sVisibilidadTexture = mTextureView.getVisibility();
        boolean camaraActiva = sVisibilidadTexture == View.VISIBLE;

        //Ocultar Botón Reproducir
        mReproducirVideo.setVisibility(View.INVISIBLE);
        tv_reproducir.setVisibility(View.INVISIBLE);

        CamaraActivity.mSalir.setVisibility(View.INVISIBLE);
        CamaraActivity.tv_salir.setVisibility(View.INVISIBLE);

        //Ocultar Botón Guardar
        mVideoTerminado.setVisibility(View.INVISIBLE);
        tv_guardar.setVisibility(View.INVISIBLE);



        //Ocultar Botón Grabar
        mButtonVideo.setVisibility(View.INVISIBLE);
        tv_iniciar.setVisibility(View.INVISIBLE);

        //Si la cámara está activa
        if (camaraActiva) {
            mTextureView.setVisibility(View.INVISIBLE);
            mVideoPrevisualizacion.setVisibility(View.VISIBLE);
        }


        /**
         * Configurar previsualizacion del video del trabajador
         */
        Uri rutaVideo = Uri.parse(mNextVideoAbsolutePath);
        mVideoPrevisualizacion.setVideoURI(rutaVideo);

        mVideoPrevisualizacion.start();

        //Cronometro
        cronometro.setFormat("  Reproduciendo: %s ");

        //Para resetear el cronometro al iniciar la grabación
        cronometro.setBase(SystemClock.elapsedRealtime());

        //Cronometro visible
        cronometro.setVisibility(View.VISIBLE);
        cronometro.setBackgroundColor(Color.TRANSPARENT);
        cronometro.setTextColor(Color.BLACK);

        //Iniciando el cronómetro
        cronometro.start();

        //Cuando se termina la reproducción del video, se muestra el botón Guardar
        mVideoPrevisualizacion.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                cronometro.stop();

                //Botón Grabar
                mButtonVideo.setVisibility(View.VISIBLE);
                tv_iniciar.setVisibility(View.VISIBLE);

                //Botón Reproducir
                mReproducirVideo.setVisibility(View.VISIBLE);
                tv_reproducir.setVisibility(View.VISIBLE);
                tv_reproducir.setText("Reproducir");

                //Botón Guardar
                mVideoTerminado.setVisibility(View.VISIBLE);
                tv_guardar.setVisibility(View.VISIBLE);

                CamaraActivity.mSalir.setVisibility(View.VISIBLE);
                CamaraActivity.tv_salir.setVisibility(View.VISIBLE);

                cronometro.setVisibility(View.INVISIBLE);
                mVideoPrevisualizacion.setVisibility(View.INVISIBLE);
                mTextureView.setVisibility(View.VISIBLE);

                //Se muestra un mensaje indicando que finalizo la reproducción del video.
                /*dialogo = new Dialogo(CamaraActivity.context, getActivity(), "La reproducción del video ha finalizado. Por favor presione Guardar.",Constantes.DIALOGO_ALERTA,"",3);
                dialogo.setCancelable(false);
                dialogo.show();*/
            }
        });
    }

    /**
     * Compara dos {@code Size}s basado en sus areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.finish();
                            //System.exit(0);
                        }
                    })
                    .create();
        }
    }

    public static class ConfirmationDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(com.android.video.R.string.permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent, VIDEO_PERMISSIONS,
                                    REQUEST_VIDEO_PERMISSIONS);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    parent.getActivity().setResult(Activity.RESULT_CANCELED);
                                    parent.getActivity().finish();
                                    //System.exit(0);
                                }
                            })
                    .create();
        }
    }

    private String getVideoArchivoPath(Context context) {
        final File dir = context.getExternalFilesDir(null);

        if (nombreArchivo == null){
            nombreArchivo =  "nombre_archivo.webm";
            nombreArchivo = nombreArchivo.replace(".webm",".mp4");
        }

        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + nombreArchivo;
    }

    public boolean subirArchivo(String ipServer, ProgressDialog progressDialog)//String ipServer
    {
        FTPClient ftpClient = new FTPClient();
        BufferedInputStream bufferedInputStream = null;
        String directorio = "/sysx/progs/web/entrada/video";

        if (CamaraActivity.ipAddress != null)
        {
            ipServer = CamaraActivity.ipAddress.replace("http://", "");
        }else{
            ipServer = "10.44.172.234";
            CamaraActivity.ipAddress = "10.44.172.234";
        }


        boolean resultado_store_file = false;

        //dialogo = new Dialogo(CamaraActivity.context, getActivity(), "Por favor, espere mientras se envía el video al servidor",Constantes.DIALOGO_ALERTA,"",4);
        //dialogo.setCancelable(false);
        //dialogo.show();

        // Toast.makeText(CamaraActivity.context,"Por favor, espere mientras se envía el video al servidor", Toast.LENGTH_LONG).show();

        try {
            //ESCRIBIENDO EN EL LOG
            //sb.append("CONECTANDO AL SERVIDOR: " + ipServer);

            ftpClient.connect(ipServer, 21);

            boolean login = ftpClient.login("sysafore", "68e071cf9d0cdce309e87477a62206d1");

            while (login == false){
                login = ftpClient.login("sysafore", "68e071cf9d0cdce309e87477a62206d1");
            }

            progressDialog.setProgress(50);
            /*
            Dialogo dialogo2 = new Dialogo(CamaraActivity.context, getActivity(), "El servidor no responde. Intente de nuevo.",Constantes.DIALOGO_ALERTA,"",4);
            dialogo2.setCancelable(false);
            dialogo2.show();
            */

            //ESCRIBIENDO EN EL LOG
            // sb.append("LOGIN AL SERVIDOR: " + login);

            ftpClient.changeWorkingDirectory(directorio);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            bufferedInputStream = new BufferedInputStream(new FileInputStream(mNextVideoAbsolutePath));
            ftpClient.enterLocalPassiveMode();

            //Contador de intentos al servidor
            int i = 1;

            if (nombreArchivo == null){
                nombreArchivo =  "nombre_archivo.webm";
                nombreArchivo = nombreArchivo.replace(".webm" , ".mp4");
            }

            // Toast.makeText(CamaraActivity.context,"Por favor, espere mientras se envía el video al servidor", Toast.LENGTH_LONG).show();

            while (i <= 3)
            {
                resultado_store_file = ftpClient.storeFile(nombreArchivo, bufferedInputStream);

                //ESCRIBIENDO EN EL LOG
                //   sb.append("RESULTADO DEL ENVIO ftpClient.storeFile: " + resultado_store_file);

                if (resultado_store_file == false) {
                    //ESCRIBIENDO EN EL LOG
                    //     sb.append("REINTENTO NUMERO " + i + ": " + resultado_store_file);
                    i++;
                } else {
                    break;
                }
            }

            //Video enviado
            if (resultado_store_file) {
                // Toast.makeText(CamaraActivity.context,"Video enviado exitosamente al servidor", Toast.LENGTH_LONG).show();
                progressDialog.setProgress(100);

                /*
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                     public void run(){

                    }
                },5000);
                   */
                //El video fue enviado al servidor y se borra del almacenamiento local.
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + nombreArchivo);
                file.delete();
                //getActivity().setResult(Activity.RESULT_OK);
                //getActivity().finish();
            }


            //Cerrando conexiones
            bufferedInputStream.close();
            ftpClient.logout();
            ftpClient.disconnect();
            //getActivity().finishAndRemoveTask();
            //System.exit(0);

        } catch (Exception e) {
            Log.i("SERVIDOR sb:  ", sb.toString());
            Log.e("SERVIDOR sb:  ", sb.toString());
        }

        Log.e("SERVIDOR sb:  ", sb.toString());

        return resultado_store_file;
    }

    private void rotarVideo() {

        apiInterface = APIClient.getClient(ipAddress).create(APIInterface.class);
        String var = infoVideo.createVideo(nombreArchivo);

        try {
            Call<rotarVideo> call = apiInterface.rotarVideo(var);
            call.enqueue(new Callback<rotarVideo>() {
                @Override
                public void onResponse(Call<rotarVideo> call, Response<rotarVideo> response) {

                    /*rotarVideo resource = response.body();
                    Integer estatus = resource.estatus;
                    String descripcion = resource.descripcion;
                    String respuesta = resource.respuesta;*/


                }

                @Override
                public void onFailure(Call<rotarVideo> call, Throwable t) {
                    call.cancel();
                }
            });

        }catch (Exception e){
            //writeLog("Exception e.toString() " + e.toString());
            Log.i("Exception:  ", e.toString());
        }

        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();

    }

    private class FTPUpload extends AsyncTask<String, Void, Void>
    {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            this.progressDialog = new ProgressDialog(CamaraActivity.context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.setMessage("Publicando...");
            this.progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            progressDialog.setProgress(30);
            subirArchivo(CamaraActivity.ipAddress,progressDialog); //"http://10.44.172.234"

            return null;
        }

        protected void onProgressUpdate(String... progress) {
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            rotarVideo();
            //getActivity().setResult(Activity.RESULT_OK);
            //getActivity().finish();

        }
    }
}

