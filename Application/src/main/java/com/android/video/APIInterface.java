package com.android.video;

import com.android.video.pojo.rotarVideo;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Sergio Gil (AP Interfaces) on 01/08/19.
 */

interface APIInterface {

    @Headers({
            "Authorization:Basic YWRtaW46MTIzNA==",
            "x-api-key:1234",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("rotarVideo")
    Call<rotarVideo> rotarVideo(@Field("info") String info);

}
