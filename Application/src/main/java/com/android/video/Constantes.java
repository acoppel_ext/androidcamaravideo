package com.android.video;

/*
    @author AP Interfaces
    @verison 1.0

    Esta clase contiene las constantes globales de la aplicación.
 */

public class Constantes {
    //Tipos de Diálogos
    public static final int DIALOGO_ALERTA = 1;
    public static final int DIALOGO_CONFIRMACION = 2;

    //Etiquetas Validaciones
    public static final String etiqSalir = "¿Deseas cancelar la grabación de video del trabajador?";
    public static final String etiqBotonOk = "Aceptar";

    //Etiquetas de Botón
    public static final String etiqBotonConfirmar = "Si";
    public static final String etiqBotonCancelar = "No";
    public static final String etiqNoConexionInternet = "No es posible enviar el video al servidor. Revise su conexión a la red.";

    //Botones del menú de opciones
    public static final int activo_boton_grabar = 1;
    public static final int activo_boton_detener = 2;
    public static final int activo_boton_reproducir = 3;
    public static final int activo_boton_guardar = 4;
    public static final int activo_boton_salir = 5;
    public static final int activo_boton_regrabar = 6;

}
