package com.android.video;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class Dialogo extends Dialog {
		Context context;
		String message;
		Dialog dialog;
		int tipo;
		String opcion;
		Activity a;
		
		public Dialogo(Context context, Activity activity, String message, int tipo, String opcion, int i) {
			super(context);
			if (this.context == null)
				this.context = context;
			if (dialog == null)
				dialog = this;
			this.message = message;
			this.tipo = tipo;
			this.opcion = opcion;
			this.a = activity;

			requestWindowFeature(Window.FEATURE_NO_TITLE);

			if (CamaraVideoFragment.cronometro != null){
				//Detengo el cronómetro
				CamaraVideoFragment.cronometro.stop();
				//Para resetear el cronometro al iniciar la grabación
				CamaraVideoFragment.cronometro.setBase(SystemClock.elapsedRealtime());
			}

		}

		public void onCreate(Bundle savedInstanceState) {
		  try{
			super.onCreate(savedInstanceState);
			
				//Declaraciones
				Eventos ev = new Eventos();	
				
				LinearLayout.LayoutParams top_sup = new LinearLayout.LayoutParams(Utilerias.getMarginAnchoDialogo(context),ViewGroup.LayoutParams.WRAP_CONTENT);
			
				LinearLayout mainLayout = new LinearLayout(context);
				mainLayout.setOrientation(LinearLayout.VERTICAL);
				mainLayout.setBackgroundColor(Color.WHITE);
				mainLayout.setLayoutParams(top_sup);

			    mainLayout.addView(Componentes.getLinea(context));
			    
				LinearLayout subLayout1 = new LinearLayout(context);
				subLayout1.setBackgroundColor(Color.WHITE);
				subLayout1.setOrientation(LinearLayout.VERTICAL);
				subLayout1.setGravity(Gravity.CENTER_HORIZONTAL);			
				
				LinearLayout layoutBoton = new LinearLayout(context);
		        layoutBoton.setOrientation(LinearLayout.HORIZONTAL);
		        layoutBoton.setGravity(Gravity.CENTER);
		        
		        LinearLayout.LayoutParams ll2 = new LinearLayout.LayoutParams(Utilerias.getMarginAnchoDialogo(context),ViewGroup.LayoutParams.WRAP_CONTENT);
		        ll2.setMargins(0,15,10,20); // left, top, right, bottom
		        
				TextView tv1 = new TextView(context);
				tv1.setTextColor(Color.BLACK);
				tv1.setText(this.message);
				tv1.setTextSize(Utilerias.getSizeFontTextoGeneral(context));
				tv1.setPadding(15,15,10,0); //left, top, right, bottom
				tv1.setGravity(Gravity.LEFT);
				
				subLayout1.addView(tv1);
				
				if (this.message.equalsIgnoreCase(Constantes.etiqSalir)){
					tv1.setGravity(Gravity.CENTER_HORIZONTAL);
					tv1.setTypeface(Typeface.DEFAULT_BOLD, Typeface.NORMAL);  //En negrita
				}
				
				ScrollView sb = new ScrollView(context);
				subLayout1.setLayoutParams(ll2);
				sb.addView(subLayout1);
				mainLayout.addView(sb);
				//setContentView(mainLayout,top_sup);
				
				Button boton1 = new Button(context);
		        boton1.setTextSize(Utilerias.getSizeFontMenu(context));
		        boton1.setTypeface(Typeface.DEFAULT_BOLD, Typeface.NORMAL);  //En negrita
		        boton1.setTextColor(Color.WHITE);
		        boton1.setGravity(Gravity.CENTER);
		        boton1.setPadding(10, 10, 10, 10);  // left, top, right, bottom
		        boton1.setLayoutParams(ll2);
		        boton1.setEnabled(true);
				
				if (this.tipo == Constantes.DIALOGO_ALERTA){
					//Dialogo Alerta
					//Boton 1
			        boton1.setText(Constantes.etiqBotonOk);
      
			        //Agregando boton 1
			        Button boton = Componentes.getBoton(context,Constantes.etiqBotonOk,Gravity.CENTER,Color.WHITE,true,Utilerias.getSizeFontFooter(context));
					layoutBoton.addView(boton);
					
					//Eventos del boton Ok y cierra Dialogo
					ev.EventoOkAcepto(context,this.a,dialog,boton,opcion);
			        
				}else{
					if (this.tipo == Constantes.DIALOGO_CONFIRMACION)
					{	
				        boton1.setText(Constantes.etiqBotonConfirmar);
				        //Agregando boton 1
				        Button boton2 = Componentes.getBoton(context,Constantes.etiqBotonConfirmar,Gravity.CENTER,Color.WHITE,true,Utilerias.getSizeFontMenu(context));
						layoutBoton.addView(boton2);
						
				        //Boton 2
						Button boton3 = Componentes.getBoton(context,Constantes.etiqBotonCancelar,Gravity.CENTER,Color.WHITE,true,Utilerias.getSizeFontMenu(context));
					    layoutBoton.addView(boton3);
					  
					    //Eventos del boton Ok
						ev.EventoFinalizar(context,a,dialog,boton2,opcion);

				        //Evento Cancelar
				        ev.EventoCancelarDialogo(context,dialog,boton3);
					}
				}    
				
				mainLayout.addView(layoutBoton);
				setContentView(mainLayout);
				
		  }catch(Exception e){
			
		  }
			
		}
}
