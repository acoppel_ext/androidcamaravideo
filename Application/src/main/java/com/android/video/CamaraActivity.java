package com.android.video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import static com.android.video.CamaraVideoFragment.mMediaRecorder;
import static com.android.video.CamaraVideoFragment.nombreArchivo;


public class CamaraActivity extends Activity {
    public static String nombre_archivo;
    public static String ipAddress;
    private String curp;
    private String folio;
    public static String texto;
    public static Context context;
    private TextView sTexto;
    public static ImageButton mButtonVideo, mDetenerGrabacion, mReproducirVideo, mVideoTerminado, mSalir;
    public static TextView tv_iniciar, tv_detener, tv_reproducir, tv_guardar, tv_salir; //textView;
    public static ProgressBar progressBar;
    private String mNextVideoAbsolutePath;
    public static String iTiempoMinimo, iTiempoMaximo;
    public static Activity activity;

    public static boolean salir = false;

    public static boolean volver_grabar = false;

    public static int opcion_activa_app = 0;

    public static boolean video_valido_min_time = false;

    public static long contador = 0;

    public static Activity a;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);

        context = this;
        activity = getParent();

        //Obteniendo los parámetros de entrada
        //Bundle parametros = this.getIntent().getExtras();
        Intent intent = getIntent();

        if(intent.hasExtra("nombrevideo"))
        //if (parametros!= null)
        {
            //if (!parametros.isEmpty())
            //{
                nombre_archivo = intent.getStringExtra("nombrevideo");
                ipAddress = intent.getStringExtra("encoioui");
                curp = intent.getStringExtra("curp");
                folio = intent.getStringExtra("folio");
                texto = intent.getStringExtra("textovideo");
                iTiempoMinimo = intent.getStringExtra("iTiempoMinimo");
                iTiempoMaximo = intent.getStringExtra("iTiempoMaximo");
            /*}else{
                nombre_archivo =  "nombre_archivo.webm";
                ipAddress = "http://10.44.172.234";
                iTiempoMinimo = "2";
                iTiempoMaximo = "120";
                texto = "Yo <<TRABAJADOR>>, y Número celular <<CELULAR>>, manifiesto que es mi voluntad traspasar mi cuenta individual de <<AFORECEDENTE>>.";
            }*/
        }else{
            nombre_archivo =  "nombre_archivo.webm";
            ipAddress = "http://10.44.172.234";
            iTiempoMinimo = "2";
            iTiempoMaximo = "120";
            texto = "Yo <<TRABAJADOR>>, y Número celular <<CELULAR>>, manifiesto que es mi voluntad traspasar mi cuenta individual de <<AFORECEDENTE>>.";
        }

        if (null == savedInstanceState)
        {

            nombre_archivo = nombre_archivo.replace(".webm" , ".mp4");

            Bundle bundle = new Bundle();
            bundle.putString("nombre_archivo", nombre_archivo);
            bundle.putString("texto", texto);
            bundle.putString("ipAddress", ipAddress);

            CamaraVideoFragment fragobj = new CamaraVideoFragment();
            fragobj.setArguments(bundle);
            getFragmentManager().beginTransaction().replace(R.id.container, fragobj).commit();

        }

        mSalir = (ImageButton) findViewById(R.id.salir);
        tv_salir = (TextView) findViewById(R.id.textoSalir);



    }

    @Override
    public void onBackPressed()
    {
        if (CamaraVideoFragment.mIsRecordingVideo){
            CamaraVideoFragment cv = new CamaraVideoFragment();
            cv.ocultarBotonDetenerGrabacion();
            cv.stopRecordingVideo();
        }

        Dialogo dialogo = new Dialogo(CamaraActivity.context,this,Constantes.etiqSalir,Constantes.DIALOGO_CONFIRMACION,"",0);
        dialogo.setCancelable(false);
        dialogo.show();
    }

    @Override
    public void onRestart(){
        super.onRestart();
    }

}

