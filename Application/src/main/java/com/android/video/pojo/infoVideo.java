package com.android.video.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergio Gil (AP Interfaces) on 26/07/19.
 */

public class infoVideo {

    @SerializedName("sNombre")
    public String sNombre;

    public infoVideo(String sNombre) {
        this.sNombre = sNombre;
    }

    //creates a json-format string
    public static String createVideo(String sNombre){
        sNombre = sNombre.replace(".mp4","");
        String info = "{\"sNombre\":\""+sNombre+"\"}";

        return info;
    }
}
